package launchpad.java.class7;


import java.util.HashMap;
import java.util.HashSet;

public class HashMapDemo {
    public static void main(String[] args) {
        // create a HashMap key is int, value is String
        HashMap<Integer,String>JohnCena = new HashMap<Integer,String>();
        HashSet<Integer> JohnCena2 = new HashSet<Integer>();
        // add (1, "one"), (2, "two"), (3, "three")
        JohnCena.put(1,"One");
        JohnCena.put(2,"Two");
        JohnCena.put(3,"Three");
        JohnCena.put(4,"Three");
        JohnCena.put(4,"Duplicate");
        // print the value "Three"
        System.out.println(JohnCena.get(3));
        // check if 2 is contained in the map? if contained, print yes, otherwise, print no
        if(JohnCena.containsKey(2)) {
            System.out.println("Yes");

        }else{
            System.out.println("No");
        }

        // remove the record (3, "Three") from the map
        JohnCena.remove(3);
    if(JohnCena.containsValue("Three")) {
        System.out.println("Three is Still There");
    }else{
        System.out.println("Three has sucessfully been removed!!!");
    }
        System.out.println(JohnCena);

    }

}
