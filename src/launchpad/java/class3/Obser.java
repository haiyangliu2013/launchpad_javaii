package launchpad.java.class3;

import java.util.Observable;
import java.util.Observer;

 public class Obser extends Observable {
    public void priceUp(){
        setChanged();
        super.notifyObservers("High");
    }

    public void priceDown(){
        setChanged();
        super.notifyObservers("Down");
    }
}

class Poor1 implements Observer{

    @Override
    public void update(Observable o, Object arg) {
        String message = arg.toString();
        System.out.println(message+"......");
    }
}

 class ObserverTest{
    public static void main(String[] args) {
        Obser observable = new Obser();
        observable.addObserver(new Poor1());
        observable.priceUp();
        observable.priceDown();
    }
}
