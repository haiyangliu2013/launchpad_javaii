package launchpad.java.class3;


public class Array {
    // get the result by multiply all elements in an array
    public static void main(String[] args) {
        // 1 bit < 1 byte < mb < gb < tb
        // 1 byte = 1024 bit
        // 1 mb = 1024 byte
        // ...

        // int : 4 byte
        // char : 1 byte

        // array1 : [0,0,0]
        int [] array1 = new int[3]; // 12 byte  --> 3 int or 12 char

        // array2 : [1,2,3]
        int [] array2 = {1,2,3};


        int [] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        //      index 0,   1,  2, 3, 4,   5 .......   9
        //System.out.println(arr.length);
        // print 3rd element
        //System.out.println(arr[7]); //

        int result = 1; // store the result
        for(int x = 0; x< arr.length; x++ ){
            // 1. x = 0 --> 0 < arr.length ?  -->  print arr[0] --> x++
            // 2. x = 1 --> 1 < arr.length ?  -->  print arr[1] --> x++
            // 3. x = 2 --> 2 < arr.length ?  -->  print arr[2] --> x++
            // ...
            // 10. x = 9 --> 9 < arr.length ? --> print arr[9] --> x++
            // 11. x = 10 --> 10 < arr.length ? --> stop and jump out the loop
            System.out.println(arr[x]);
            result = result * arr[x];
            // 1. x = 0, result = 1 --> result = 1 * arr[0] = 1 --> x++
            // 2. x = 1, result = 1  --> result = 1   * arr[1] = 2 --> x++
            // 3. x = 2 , result = 2  --> result = 2   * arr[2] = 6 --> x++
            // ...
        }
        System.out.println(result);


    }
}
