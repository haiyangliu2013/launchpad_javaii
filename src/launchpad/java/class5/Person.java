package launchpad.java.class5;

public class Person {
    public String name;  // can access by anyone and anywhere
    protected int age; // can only access by itself or its child
    private String job; // can access whihin the same package (default)
    private double salary; // can only access by itself within the class

    // Overloading
    public Person() {} // construtor

    public Person(String name, int age, String job, double salary) {
        this.name = name;
        this.age = age;
        this.job = job;
        this.salary = salary;
    }

    public void gotoWork(){
        System.out.println(name + " goes to work!");
    }

    // define a method : getSalary, param: double salary, print "your salary is ...";
    public void getSalary() {

        System.out.println(name + "'s salary is $"+salary);
    }

    public void learn(String book){
        System.out.println(name + " is learning "+ book);
    }
}

class Student extends Person{
    private String school;

    public Student(String name, int age, String school){
        this.name = name;
        this.age = age;
        this.school = school;
    }

         public void school() {
             System.out.println(name + " is from " + school);
         }

    public void learn(String book){
        System.out.println("Student "+ name + " is learning " + book);
    }
}

class Driver{
    public static void main(String[] args) {

        Person jack = new Person("Jack", 9, "developer", 4000);
        jack.gotoWork();
        jack.getSalary();

        Student s = new Student("Peter", 10, "Jeff");
        s.learn("English");
        s.school();

    }
}
