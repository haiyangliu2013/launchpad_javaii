package launchpad.java.class5;

public class Animal {
    // attributes for Animal class
    // 1. weight
    // 3. name
    // 4. color
    // 5. canFly
    // 6. canSwim

//    double weight;
//    String name;
//    String color;
//    boolean canFly;
//    boolean canSwim;

    // behavior
    // 1. move
    // void: do not require returning
    // other return types: int, double, String, boolean, int[], ....
//    int move(){
//        return 0;
//    }

    public double weight;
    public String area;
    private String name;
    protected String color;
    private boolean canFly;
    protected boolean canSwim;

    // 1. getWeight(): should print the weight from the attribute
    // 2. flyOrSwim(): if canFly is true, print "it can fly", otherwise "it can't fly"
    //                    if canSwim is true, print "it can swim", otherwise "it can't swim"

    public void getWeight(double weight){
        System.out.println("your weight is "+weight);
    }
    public void canFlyorSwim(boolean CanFlyorSwim){
        System.out.println(": "+CanFlyorSwim);
    }

}
