package launchpad.java.class5;

public class Vehicle {
    public static int WHEELS = 4;
    public  String brand;



    public String model;

    protected int year;
    protected double price;

    private int mileage;
    private int seats;

    public Vehicle() {}

    public Vehicle(String brand, String model, int year, double price, int mileage, int seats) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.price = price;
        this.mileage = mileage;
        this.seats = seats;
    }

    public double estimatePrice(){
        double newPrice = this.price - (2018-this.year)*2000;
        return newPrice;
    }


    public String printInfo() {
        return "Vehicle " +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", mileage=" + mileage +
                ", seats=" + seats
                ;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
}

class VehicleMain{
    public static void main(String[] args) {
        // Static variable belongs to class level, can be accessed by class directly
        // Non-Static variable belongs to object level, is accessed by object.
        System.out.println(Vehicle.WHEELS);
        Vehicle v1 = new Vehicle();
        System.out.println(v1.brand);

        System.out.println(Vehicle.WHEELS);
        //Vehicle v1 = new Vehicle();
        Vehicle v2 = new Vehicle("BMW", "X6", 2017, 60000.0, 1000, 7);
    }
}
