package launchpad.java.class5;

public class StaticAndFinal {
    public static void main(String[] args) {
        // WHEELS is static variable, so we can access it directly without creating a new Vehicle object
        System.out.println(Vehicle.WHEELS);

        // for final object, we can change its inner value, but we can't change its reference
        // which means once the object assigned, we can't assign another object to this object.
        final Person p1 = new Person("jack", 9, "developer", 3000);
        Person p2 = new Person("Peter", 10, "student", 3000);
        //p1 = p2;
        p1.name = "Jerry";
    }
}
