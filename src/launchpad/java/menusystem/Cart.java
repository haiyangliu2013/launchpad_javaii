package launchpad.java.menusystem;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List<Item> cart;
    public Cart() {
        this.cart = new ArrayList<>();
    }

    public void addToCart(Item item){
        cart.add(item);
    }

    public Item removeFromCart(String name){
        int index = -1;
       for(int i=0; i<cart.size(); i++){
           if(cart.get(i).getName().equals(name)){
               index = i;
               break;
           }
       }
       return cart.remove(index);
    }

    public void printCart(){
        for(Item item : cart){
            System.out.println(item.toString());
        }
    }
}
