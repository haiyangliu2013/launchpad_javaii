package launchpad.java.menusystem;

public class CartDriver {
    public static void main(String[] args) {
        Cart cart = new Cart();
        Item item1 = new Item("cheese", 5.99);
        Item item2 = new Item("water", 1.99);
        Item item3 = new Item("milk", 3.99);
        cart.addToCart(item1);
        cart.addToCart(item2);
        cart.addToCart(item3);
        cart.printCart();
        cart.removeFromCart("cheese");
        cart.printCart();
    }
}
