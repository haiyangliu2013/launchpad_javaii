package launchpad.java.finalproject;

import java.util.Scanner;

public class SteamMain {
    public static void main(String[] args) {
        // define the menu String
        //  \n means new line
        String menu = "\tA) Add Game to shopping cart \n" +
                      "\tB) Get Game \n" +
                      "\tC) Remove Game from shopping cart \n" +
                      "\tD) View Shopping Cart \n" +
                      "\tE) Purchase \n" +
                      "\tF) View My Game\n" +
                      "\tQ) Quit My Steam";
        String selection = "Selece an option: ";
        // Scanner is used for user input
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to MySteamApp!");
        System.out.println("Please enter your username: ");
        String username = sc.nextLine();
        System.out.println("Please enter your password: ");
        String password = sc.nextLine();
        System.out.println("Welcome " + username);
        SteamAccount sa = new SteamAccount(username, password);
        // use while loop to keep displaying the menu
        while(true){
            System.out.println(menu);
            System.out.println(selection);
            String option = sc.nextLine().toUpperCase();
            switch (option){
                case "A":
                    System.out.println("Add Game Chosen.");
                    System.out.println("Please input the name: ");
                    String name = sc.nextLine();
                    System.out.println("Please input the type: ");
                    String type = sc.nextLine();
                    System.out.println("Please input the price: ");
                    String price = sc.nextLine();
                    Game newGame = new Game(name, type, price);
                    // Question: How to add newGame to shopping cart?
                    // answer:
                    sa.addToCart(newGame);
                    break;
                case "B":
                    System.out.println("Get Game Chosen.");
                    System.out.println("Please input the name: ");
                    String searchName = sc.nextLine();
                    Game searchGame = sa.getGame(searchName);
                    if(searchGame !=null){
                        System.out.println(searchGame.getName()+", "+
                                            searchGame.getType()+", "+
                                            searchGame.getPrice());
                    }
                    break;
                case "C":
                    System.out.println("Remove Game Chosen.");
                    System.out.println("Please input the name: ");
                    String removeName = sc.nextLine();
                    sa.removeFromCart(removeName);
                    break;
                case "D":
                    System.out.println("View Shopping Cart Chosen.");
                    sa.viewShoppingCart();
                    break;
                case "E":
                    System.out.println("Purchase Chosen.");
                    sa.purchase();
                    break;
                case "F":
                    System.out.println("View My Game Chosen.");
                    sa.viewMyGames();
                    break;
                case "Q":
                    System.out.println("Quit Chosen.");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Option is invalid, please try again.");
            }
        }


    }
}
