package launchpad.java.finalproject;

import java.util.ArrayList;
import java.util.List;

public class SteamAccount {
    private String username;
    private String passWord;
    private List<Game> shoppingCart;
    private List<Game> myGame;

    // every time we create a steam account, we will create 2 new list inside,
    // one is shopping cart list
    // one is my game list
    public SteamAccount(String username, String passWord) {
        this.username = username;
        this.passWord = passWord;
        shoppingCart = new ArrayList<>();
        myGame = new ArrayList<>();
    }

    public Game getGame(String name){
        for(int i=0; i<shoppingCart.size(); i++){
            if(shoppingCart.get(i).getName().equals(name))
                return shoppingCart.get(i);
        }
        System.out.println("Game not found");
        return null;
    }

    public void addToCart(Game game){
        shoppingCart.add(game);
    }

    public void removeFromCart(String name){
        for(int i=0; i<shoppingCart.size(); i++){
            if(shoppingCart.get(i).getName().equals(name)) {
                System.out.println(name + " has been removed from your cart");
                shoppingCart.remove(i);
                return;
            }
        }
        System.out.println("Game not found in your cart.");
    }

    public void purchase(){
        myGame.addAll(shoppingCart);
        shoppingCart.clear();
        System.out.println("Purchase Successfully.");
    }

    public void viewShoppingCart(){
        for(int i=0; i<shoppingCart.size(); i++){
            System.out.println(i + "  " +
                                shoppingCart.get(i).getName() + ", " +
                                shoppingCart.get(i).getType() + ", "+
                                shoppingCart.get(i).getPrice()
            );
        }
    }

    public void viewMyGames(){
        for(int i=0; i<myGame.size(); i++){
            System.out.println(i + "  " +
                    myGame.get(i).getName() + ", " +
                    myGame.get(i).getType() + ", "+
                    myGame.get(i).getPrice()
            );
        }
    }


}
