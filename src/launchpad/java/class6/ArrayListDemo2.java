package launchpad.java.class6;
import java.util.ArrayList;

public class ArrayListDemo2 {
    public static void main(String[] args) {
        ArrayList <String> arraylist = new ArrayList<>();
        arraylist.add("apple");
        arraylist.add("banana");
        arraylist.add("cat");
        arraylist.add("Peter");

        arraylist.remove("cat");
        for (int x = 0;x < arraylist.size() ; x++){
            if (arraylist.get(x) == "Peter"){
                System.out.println( arraylist.get(x) );
            }
        }
    }
}
