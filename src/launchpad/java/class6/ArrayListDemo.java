package launchpad.java.class6;
import java.util.ArrayList;

public class ArrayListDemo {
    // 1. create an arraylist which accept String type as data
    // 2. add "apple", "banana", "cat", "Peter"
    // 3. remove "cat"
    // 4. select the "Peter" and print out only "Peter", use for loop
    public static void main(String[] args) {
        ArrayList<String> arraylist = new ArrayList<>();

        arraylist.add("21");
        arraylist.add("peter");
        arraylist.add("mouse");
        System.out.println(arraylist.get(2));
        for(int i=0; i<arraylist.size(); ++i){
            if(arraylist.get(i).equals("peter"))
                System.out.println(arraylist.get(i));
        }
        System.out.println(arraylist);




        // Integer is wrap class of int
        // primitive data type: int, double, float, boolean
        // wrap class of primitive: Integer, Double, Float, Boolean


//.println("get(0): " + e);

        // create an arraylist which accept String as data
        // add "apple", "banana", "cat", "Peter"
        // remove "cat"
        // select the "Peter" and print out only "Peter", by loop

//        ArrayList<String> Things = new ArrayList<String>();
//        Things.add("apple");
//        Things.add("banana");
//        Things.add("cat");
//        Things.add("peter");
//        Things.remove("cat");
//
//        for(int i=0;i<Things.size();i++) {
//            if(Things.get(i).equals("peter")) {
//                System.out.println(Things.get(i));
//            }
//        }
//
//        System.out.println(Things);


    }
}
