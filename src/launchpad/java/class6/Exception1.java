package launchpad.java.class6;

public class Exception1 {
    private static int  div(int a, int b) throws ArithmeticException {
        if(b == 0) {
            throw new ArithmeticException("Division by Zero");
        } else {
            return a / b;
        }
    }

    public static void main(String[] args) {
       //div(3,0);
        try {
            System.out.println(3 / 1);
            System.out.println("Division is valid");
            int [] arr = new int[3];
            System.out.println(arr[6]);
        }catch (ArithmeticException e){
            System.out.println("It is not possible to / by zero");
        }

    }
}
