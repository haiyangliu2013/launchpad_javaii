package launchpad.java.class4;

// What is Multidimensional Arrays?
// It is array inside array, you can access sub-array by using multiple "[]"
public class MDArray {
    public static void main(String[] args) {
        int [] a = {1,2,3,4,5};
        int [][] arr = {{4,6,8},{32,43,12},{76,23,12},{17}};
        System.out.println(arr[1][1]);


//        System.out.println(a[3]);
//        System.out.println(arr[1][2]);
        String [][] arr2 = {{"apple", "Banana", "Peach"},{"Allen", "Bob", "Jason"},{"Cat", "Dog", "Mouse"}};

        // print "Banana", "Jason", "Mouse"

        int arr3[ ] = new int[3];
        for (int i = 0; i < 3; i++) {
            arr3[i] = i;
            System.out.println("arr3["+i+"] is: "+arr3[i]);
        }
        int res = arr3[1] + arr3[2];
        System.out.println(res);
    }
}
