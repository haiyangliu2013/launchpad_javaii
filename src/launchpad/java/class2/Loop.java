package launchpad.java.class2;

public class Loop {
    public static void main(String[] args) {
        // write a loop , either while loop or for loop
        // print out all even number from 0 - 100
        int counter = 0;
        while (counter < 100){
            //System.out.println("in while loop");
            // if a number can be divided by 2, that means the number is even number, x % 2 == 0
            // if a number can't be divided by2, that means the number is odd number, x % 2 != 0
            if (counter % 2 == 0){
                System.out.println(counter);
            }

            counter++;
        }


    }
}
