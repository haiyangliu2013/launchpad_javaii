package launchpad.java.class2;

public class Switch {
    // use given enum DAY, write switch statement
    // to print corresponding "Today is Monday" for example;
    // default case: "Today is Weekend"
    enum DAY {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday,
    }
    // create main method
    public static void main (String [] args){
        DAY day = DAY.Sunday;
        switch (day){
            case Monday:
                System.out.println("Today is Monday");
                break;
            case Tuesday:
                System.out.println("Today is Tuesday");
                break;
            case Wednesday:
                System.out.println("Today is Wednesday ");
                break;
            case Thursday:
                System.out.println("Today is Thursday ");
                break;
            case Friday:
                System.out.println("Today is Friday ");
                break;
            default:
                System.out.println("Today is weekend");
        }

    }


}


































//    public static void main(String[] args) {
//        DAY day;
//        day = DAY.Monday;
//        switch (day){
//            case Monday:
//                System.out.println("Today is Monday");
//                break;
//            case Tuesday:
//                System.out.println("Today is Tuesday");
//                break;
//            case Wednesday:
//                System.out.println("Today is Wednesday");
//                break;
//            case Thursday:
//                System.out.println("Today is Thursday");
//                break;
//            case Friday:
//                System.out.println("Today is Friday");
//                break;
//            default:
//                System.out.println("Today is Weedend");
//                break;
//        }
//    }

//}
