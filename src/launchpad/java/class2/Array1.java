package launchpad.java.class2;

public class Array1 {
    public static void main(String[] args) {
        // create an array with 100 slots
        // assign the array with all the odd number 1....199
        int[] Array2 = new int[100];

        int cursor = 0;
        for(int i = 0; i<200; i+=1) {

            if(i%2 != 0) {
                Array2[cursor] = i;
                cursor++;
            }

        }

        for(int n : Array2){
            System.out.print(n+" ");
        }
    }
}
